//CPE103 - Gharibyan - Project 2
//Andy Chen & Michael Woodson
//HeapTest Class:
//Tests the methods in the BinHeap class and make sure they're functioning 
//correctly. In the end output heap contents by using deletemin.

import java.util.*;

public class HeapTest {
   
   public static void main(String[] args) {
      
      Scanner input = new Scanner(System.in);
      char choice = 'z';
      String temp;  //A variable that temporarily stores user input string
      
      System.out.print("Enter the heap size: ");
      int size = input.nextInt();
      input.nextLine();
      
      //Initialize a heap for storing Strings
      BinHeap<String> heap = new BinHeap<String>(size);
      
      System.out.println("Choose one of the following operations:");
      System.out.println("- add an element (enter the letter a)");
      System.out.println("- delete the smallest element (enter the letter d)");
      System.out.println("- is the heap empty (enter the letter e)");
      System.out.println("- size of the collection (enter the letter s)");
      System.out.println("- print the collection (enter the letter p)");
      System.out.println("- Quite (enter the letter q)");
      
      while(choice != 'q')
      {
         System.out.println("Enter choice: ");
         choice = input.nextLine().charAt(0);
   
         switch (choice)
         {
            case 'a':
               System.out.println("Please enter string to be added: ");
             
               temp = input.nextLine();
               heap.insert(temp);
               System.out.println(temp + " inserted");
               
               break;
            case 'd':
               try
               {
                  temp = heap.deleteMin();
                  System.out.println(temp + " deleted");
               }
               catch (BinHeap.MyException e)
               {
                  System.out.println("Invalid operation on an empty heap");
               }
               break;
            case 'e':
               if(heap.isEmpty())
                  System.out.println("heap is empty");
               else
                  System.out.println("heap is not empty");
               break;               
            case 's':
               System.out.println("the size is: " + heap.size());
               break;
            case 'p':             
               System.out.println(heap.toString());
               break;
            case 'q':
                  System.out.println("Quitting");
               break;
            default:
               System.out.println("Invalid choice");
               break;     
         }
      }
      
      //Print out contents of heap
      while(!heap.isEmpty())
         System.out.print(heap.deleteMin() + " ");
      
   }

}
