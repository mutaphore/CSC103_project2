//CPE103 - Gharibyan - Project 2
//Andy Chen & Michael Woodson
//BinHeap Class:
//Heap abstract data structure generic class implemented with array
//Contains Insert, Deletemin, isEmpty, size and toString methods

public class BinHeap <T extends Comparable <? super T>> {

   //Nested exception class to indicate if a error with heap occurred   
   public static class MyException extends RuntimeException {
      public MyException() {
         super();
      }
      public MyException(String msg) {
         super(msg);
      }
   }
      
   private T[] arry;
   private int heapSize;
   
   //Default Constructor with no parameter
   public BinHeap() {
      arry = (T[]) new Comparable[100];
      heapSize = 0;
   }
   
   //Constructor initializes private instance variables
   //parameters:
   //arrySize - size to initialize the array to.
   public BinHeap(int arrySize) {
      arry = (T[]) new Comparable[arrySize];
      heapSize = 0;
   }
   
   //Insert method:
   //Inserts an element into heap and rearranges to the correct heap structure
   //where and element is always smaller than it's children.
   //parameters: data - the value of the element to be inserted into heap
   public void insert(T data) {
      
      int hole;
      int parent;
      
      if(heapSize == arry.length) {
         T[] temp = (T[]) new Comparable[arry.length*2];
         for(int i=0; i<arry.length; i++)
            temp[i] = arry[i];
         arry = temp;
      }
      
      hole = heapSize;
      parent = (hole-1)/2;

      while(hole != 0 && arry[parent].compareTo(data) > 0) {
         arry[hole] = arry[parent];
         hole = parent;
         parent = (hole-1)/2;
      }
      
      arry[hole] = data;
      heapSize++;

   }
   
   //DeleteMin method:
   //Removes the smallest element in the heap and returns it.
   //return: value of the smallest element
   public T deleteMin() {
      
      if(heapSize > 0) {
         T returnVal = arry[0];
         T item = arry[heapSize-1];
         heapSize--;
         int hole=0;
         int newHole;
         
         newHole = newHole(hole,item);
         while(newHole != -1) {
            arry[hole] = arry[newHole];
            hole = newHole;
            newHole = newHole(hole,item);
         }
         arry[hole] = item;
         
         return returnVal;
      }
      else {
         throw new MyException();
      }
      
   }
   
   //newHole method:
   //Support private method for deleteMin. Finds the next hole for the current
   //hole to move to and returns its index. Otherwise return -1
   //parameters: hole - current hole index
   //            item - value of the last element in heap that will go into hole
   //return: index of newhole or -1 if a move is not necessary
   private int newHole(int hole, T item) {
      
      int newHole = -1;
      int smaller;
           
      if(2*hole+1 < heapSize) {        //hole has children
         if(2*hole+2 >= heapSize) {    //hole has no right child, only left child
            if(arry[2*hole+1].compareTo(item) < 0) {     
               newHole = 2*hole+1;
            }
         }
         else {                        //hole has both children
            if(arry[2*hole+1].compareTo(arry[2*hole+2]) > 0) {    //find the smaller child
               smaller = 2*hole + 2;
            }
            else
               smaller = 2*hole + 1;            
            if(arry[smaller].compareTo(item) < 0) {
               newHole = smaller;               
            }
         }
      }

      return newHole;
      
   }
   
   //isEmpty method:
   //Checks to see if the heap is empty
   //return: true if heap is empty and false if it isn't
   public boolean isEmpty() {
      
      return heapSize == 0;
      
   }
   
   //size method:
   //returns the heap size
   public int size() {
      
      return heapSize;
      
   }
   
   //toString method:
   //overrides toString, prints heap array elements in order separated by spaces
   //return: A string that contains the array elements
   public String toString() {
      
      String temp = "";
      
      for(int i=0; i<heapSize; i++) {
         temp = temp.concat(arry[i].toString() + " ");
      }
      
      return temp;
      
   }
   
   
}
