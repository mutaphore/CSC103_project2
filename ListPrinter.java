//CPE103 - Gharibyan - Project 2
//Andy Chen & Michael Woodson
//ListPrinter Class:
//Outputs a sorted list of student records, in ascending order.
//Entering data from a file. Sorting is by student id. Invalid record is not inserted.
//A record is considered invalid, if at least one of the following 4 conditions occurs:
//1. the id is not a long type value (note that any integer is a long type value)
//2. the id is not a positive value (it is either 0 or a negative number)
//3. the id and/or the name are missing (the number of values on the line is less than 2) 
//4. there are additional values on the line (more than 2 values on the line).

import java.util.*;
import java.io.*;

public class ListPrinter {
   
   public static void main(String[] args) {

      Scanner input = new Scanner(System.in);
      BinHeap<Student> studentHeap = new BinHeap<Student>();
      long  tempID; //Temporarily stores the student ID
      String tempName; //Temporarily store the student Name
      Scanner lineScanner; //Scans 1 line for ID and name
      
      System.out.print("Please enter filename: ");
      File reader = new File(input.nextLine()); //Reads 1 line from file
      
      try {     
         Scanner fileIn = new Scanner(reader); 
         while(fileIn.hasNextLine()) {
            lineScanner = new Scanner(fileIn.nextLine());
            //Make sure the first token is student ID and it's > 0
            if(lineScanner.hasNextLong() && (tempID = lineScanner.nextLong()) > 0) {
               if(lineScanner.hasNext()) {
                  tempName = lineScanner.next();
                  //Make sure no more than 2 tokens per line or skip
                  if(!lineScanner.hasNext())
                     studentHeap.insert(new Student(tempID,tempName));                  
               }               
            }
            lineScanner.close();
         }
      }
      catch(IOException e) {
         System.out.print("File not found!");
      }
      
      //Outputs a sorted (ascending) list of students.
      System.out.println("Student List: ");
      for(int i=1; !studentHeap.isEmpty(); i++) {
         System.out.println(i + ". " + studentHeap.deleteMin().toString());         
      }
      
   }

}
