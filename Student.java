//CPE103 - Gharibyan - Project 2
//Andy Chen & Michael Woodson
//Student Class:
//A student object represents one student's information record that contains
//the student ID, and student last name  

public class Student implements Comparable<Student> {

   private long studentID;
   private String lastName;
   
   //Constructor that initializes the Student ID and last name variables
   public Student(long studentID, String lastName) {
      
      this.studentID = studentID;
      this.lastName = lastName;      
   
   }
   
   //Method compareTo:
   //Implements the comparable method.
   //parameter: other - another student object
   //return: 1 if current > other, -1 if current < other, 0 if equal
   public int compareTo(Student other) {
      
      if(this.studentID > other.studentID)
         return 1;
      else if (this.studentID == other.studentID)
         return 0;
      else
         return -1;
      
   }
   
   //Method toString:
   //Overrides the toString method.
   //Return: a formatted String with student ID and last name
   public String toString() {
      
      String temp = "";
      return temp.concat("{ id: " + this.studentID + ", name: " + this.lastName + " }"); 
   
   }
   
}
